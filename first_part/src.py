def exercise_one(number):
    for i in range(1, number + 1):
        # Display `ThreeFive`  for multiples of both 3 and 5
        if i % 3 == 0 and i % 5 == 0:
            print('ThreeFive')
        # Display `Three`  for multiples of 3
        elif i % 3 == 0 and i % 5 != 0:
            print('Three')
        # Display `Five`  for multiples of 5
        elif i % 5 == 0 and i % 3 != 0:
            print('Five')
        # Otherwise, display other numeric in the range
        else:
            print(i)


def find_missing_nb(arr):
    array_length = len(arr)
    total = (array_length + 1) * (array_length + 2) // 2
    summation = sum(arr)
    return total - summation


def sort_array_unmoved_negative(array, numbers):
    # Non-negative values
    output = list()
    for number in range(numbers):
        if array[number] >= 0:
            output.append(array[number])

    # Sorting Non-negative values
    output = sorted(output)

    # Updating the position of the Non-Negative Elements
    p = 0
    for q in range(numbers):
        if array[q] >= 0:
            array[q] = output[p]
            p += 1

    # Displaying the unmoved negative sorted array
    result = list()
    for x in range(numbers):
        result.append(array[x])
    return result

