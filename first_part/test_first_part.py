from src import sort_array_unmoved_negative, find_missing_nb, exercise_one


test_output = "1\n2\nThree\n4\nFive\nThree\n7\n8\nThree\nFive\n11\nThree\n13\n14\nThreeFive\n16\n17\nThree\n19\nFive" \
              "\nThree\n22\n23\nThree\nFive\n26\nThree\n28\n29\nThreeFive\n31\n32\nThree\n34\nFive\nThree\n37\n38" \
              "\nThree\nFive\n41\nThree\n43\n44\nThreeFive\n46\n47\nThree\n49\nFive\nThree\n52\n53\nThree\nFive\n56" \
              "\nThree\n58\n59\nThreeFive\n61\n62\nThree\n64\nFive\nThree\n67\n68\nThree\nFive\n71\nThree\n73\n74" \
              "\nThreeFive\n76\n77\nThree\n79\nFive\nThree\n82\n83\nThree\nFive\n86\nThree\n88\n89\nThreeFive\n91\n92" \
              "\nThree\n94\nFive\nThree\n97\n98\nThree\nFive\n"


def test_first_exercise(capsys):
    exercise_one(100)
    captured = capsys.readouterr()
    assert captured.out == test_output


def test_missing_number():
    # Created Testing Arrays: Array 1 & 2 are missing a number and output correctly
    array1 = [8, 2, 3, 6, 4, 7, 1]
    array2 = [2, 1, 3, 5]
    # Array 3 is missing two numbers: 16 & 18 and will give wrong output
    array3 = [15, 20, 17, 19]

    assert find_missing_nb(array1) == 5    # Should Pass - right output: 5
    assert find_missing_nb(array2) == 4    # Should Pass - right output: 4

    # last test outputs -56 which != 16, but for the sake of completeness, I put -56
    assert find_missing_nb(array3) == -56  # 16   # Should pass - wrong output: -56


def test_sort_mixed_array():
    # Creating test arrays
    arr1 = [2, -6, -3, 8, 4, 1]  # --> Output: 1, -6, - 3, 2, 4, 8
    arr2 = [-2, -6, -3, -8, 4, 1]  # --> Output: -2 - 6 - 3 - 8

    assert sort_array_unmoved_negative(arr1, len(arr1)) == [1, -6, -3, 2, 4, 8]
    assert sort_array_unmoved_negative(arr2, len(arr2)) == [-2, -6, -3, -8, 1, 4]
