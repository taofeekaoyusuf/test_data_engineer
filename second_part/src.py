import sys


class MySet(set):
    # todo exercise 1
    def __init__(self, set):
        self.items = set

    def __add__(self, new):
        return set(self.items + new.items)

    def __sub__(self, new):
        return set(self.items) - set(new.items)


def decorator_check_max_int(func):
    # todo exercise 2
    # define an inner function for the maxsize check
    def inner(a, b):
        if func(a, b) < sys.maxsize:
            return func(a, b)
        else:
            return sys.maxsize

    return inner  # returns maxsize status


@decorator_check_max_int
def add(a, b):
    return a + b


def ignore_exception(exception):
    # todo exercise 3
    # An inner function to pass the argument to 'ignore_exception' function
    def decorator(func):
        # declared a helper function to take the exception arguments
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except exception:
                return

        return wrapper  # returns the function to the inner function

    return decorator  # returns the exception


@ignore_exception(ZeroDivisionError)
def div(a, b):
    return a / b


@ignore_exception(TypeError)
def raise_something(exception):
    raise exception


# exercise 4
class CacheDecorator:
    """Saves the results of a function according to its parameters"""

    def __init__(self):
        self.cache = {}

    def __call__(self, func):
        def _wrap(*a, **kw):
            if a[0] not in self.cache:
                self.cache[a[0]] = func(*a, **kw)
            return self.cache[a[0]]

        return _wrap


# Exercise 5
class MetaInherList(type):
    # Initiate the new method of the metaclass
    def __new__(self, class_name, bases, attrs):
        # looping through the attrs to check if it is an obj or a list
        for key, attr in attrs.items():
            if key == object:
                attr = object()
            else:
                attr = tuple(list())
        return type(class_name, bases, {'attrs': attr})  # returns the new method creation


class Ex:
    x = 4


class ForceToList(Ex, list, metaclass=MetaInherList):
    pass


# Exercise 6
class CheckProcess(type):
    def __new__(self, class_name, bases, attrs):
        # Method process creation
        def process(*args):
            try:
                if hasattr(bases, 'process'):  # checking to see if class has 'process' attributes
                    try:
                        if len(args[3]) == 3:  # Ensuring correct number of arguments is passed to the method
                            return args
                        return type(class_name, bases, {'process': process})   # returns new method
                    except AttributeError as e:
                        print(f"\n{e}\nNo attribute name 'process' in this class\n")
            except ValueError as E:
                print(f"\n{E}\nERROR: Check the number of passed argument. 3 argument required\n")


# Simple class for testing inheriting from the metaclass CheckProcess
class SimpleClass(metaclass=CheckProcess):
    pass

