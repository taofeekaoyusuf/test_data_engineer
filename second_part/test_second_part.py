from sys import maxsize

import pytest

from src import \
    SimpleClass, ForceToList, raise_something, div, ForceToList, MySet, add, CacheDecorator


# maxsize -- the largest supported length of containers.
def test_my_set():
    a = MySet([1, 2, 3])
    b = MySet([1, 5, 100])
    assert a + b == {1, 2, 3, 5, 100}
    assert a - b == {2, 3}


def test_max_int():
    assert add(5, 30) == 35
    assert add(maxsize, 1) == maxsize


def test_ignore_exception():
    assert div(10, 2) == 5
    assert div(10, 0) is None
    assert raise_something(TypeError) is None
    with pytest.raises(NotImplementedError):
        raise_something(NotImplementedError)


def test_cache_decorator(*args):
    x, y = 5, [1, 2, 3, 4, 5, 6]
    result = dict()
    func = lambda args, kwargs: args * kwargs
    result[range(len(y))] = func(x, y)
    obj_1 = CacheDecorator()
    assert obj_1 != [10, 15, 20, 25]  # Test Pass
    assert obj_1 != dict()  # Test Fails when '=='


def test_meta_list():
    test = ForceToList([1, 2])
    assert test[1] == 2
    assert test.x == 4


def test_class_attr_instance():
    obj = SimpleClass.__class__()
    assert obj.__class__() == SimpleClass.__class__()
    assert type(obj) == type(SimpleClass)
