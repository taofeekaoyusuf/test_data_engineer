import pytest
from src import clean_cat, import_raw_data, process_data, average_prices, count_products_by_categories_by_day, \
    average_products_by_categories


def test_clean_cat():
    test_col = "idappcat"
    test_string = "2076,3B,19C,138D,NULL,NULL"
    path = "third_part/data/products.json.gz"
    assert clean_cat(test_string) != "2076,3B,19C,138D,NULL,NULL"
    assert clean_cat(path)[test_col][1] == "2076,3B,19C,138D"
    assert clean_cat(test_string) == "2076,3B,19C,138D"
    assert not clean_cat(path)[test_col].__contains__('NULL')  # Testing for NULL value


def test_import_raw_data():
    back_ofis, day_one, day_two = import_raw_data()
    assert day_one.__contains__('prixproduit')
    assert day_one.columns.all() == day_two.columns.all()
    assert day_two.__contains__('identifiantproduit')
    assert back_ofis.columns.all() != (day_one.columns.all() and day_two.columns.all())
    assert back_ofis.__contains__('pe_ref_in_enseigne')


def test_process_data():
    d1_df, d2_df = process_data()
    assert d1_df.columns.__contains__('identifiantproduit' and 'prixproduit')
    assert d1_df.columns.all() == d2_df.columns.all()
    assert d2_df.columns.__contains__('categorieenseigne' or 'refenseigne')
    assert not d2_df.columns.__contains__("")  # Testing for Blank record


def test_average_prices():
    avg_prix_df1, avg_prix_df2 = average_prices()
    assert len(avg_prix_df1) != len(avg_prix_df2)
    assert max(avg_prix_df1) == max(avg_prix_df2)
    assert min(avg_prix_df1) == min(avg_prix_df2)
    assert sum(avg_prix_df1) != sum(avg_prix_df2)  # as 30181.540000000346 != 30627.100000000373


def test_count_products_by_categories_by_day():
    di_prod_cat_d1, di_prod_cat_d2, ret_prod_cat_d1, ret_prod_cat_d2 = count_products_by_categories_by_day()
    assert di_prod_cat_d1 == di_prod_cat_d2
    assert ret_prod_cat_d1 != ret_prod_cat_d2
    assert not (di_prod_cat_d1 and di_prod_cat_d2 and ret_prod_cat_d1 and ret_prod_cat_d2).__eq__(0)
    assert not (di_prod_cat_d1 or di_prod_cat_d2 or ret_prod_cat_d1 or ret_prod_cat_d2).__lt__(0)


def test_average_products_by_categories():
    di_avg_numOfProd_d1, di_avg_numOfProd_d2, rt_avg_numOfProd_d1, rt_avg_numOfProd_d2 = average_products_by_categories()
    assert di_avg_numOfProd_d1 == di_avg_numOfProd_d2
    assert rt_avg_numOfProd_d1 != rt_avg_numOfProd_d2
    assert not (di_avg_numOfProd_d1 and di_avg_numOfProd_d2 and rt_avg_numOfProd_d1 and rt_avg_numOfProd_d2).__eq__(0)
    assert not (di_avg_numOfProd_d1 and di_avg_numOfProd_d2 and rt_avg_numOfProd_d1 and rt_avg_numOfProd_d2).__lt__(0)
