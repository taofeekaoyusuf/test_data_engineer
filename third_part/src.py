# import something if needed
import os
import pandas as pd


def clean_cat(args):
    # todo exercise 1
    # Reading and Parsing the JSON file
    try:
        # Some columns and path needed
        col = 'idappcat'
        path = os.getcwd() + '/data/'
        file_path = "products.json.gz"
        # Reading the file from disk
        product_list = pd.read_json(path + file_path)  # Total records: 131370
        # Checking if the parameter passed starts with "third" folder path
        if args.startswith("third"):
            # Operation to remove the blank records in the Dataframe
            nan_value = float("NaN")
            product_list.replace("", nan_value, inplace=True)
            product_list = product_list.dropna()  # Record after `dropna` operation: 97013

            # Operation to remove the NULL values from the `idappcat` Dataframe column
            product_list[col] = product_list[col].str.replace(',NULL', '')
            return product_list  # Return clean Dataset with more than 34,000 record lost to the cleaning process
        # Else check if the parameter is a string containing ","
        elif args.__contains__(','):
            output = args.replace(',NULL', '')  # if true clean the string
            return output  # and return a cleaned string of data
        else:
            pass
    except TypeError as e:
        print(f"\n{e}\nERROR: Invalid Parameter passed\n")


def import_raw_data():
    # todo exercise 2
    path = os.getcwd() + '/data/'  # Get the Current working directory
    # Files to be read from disk
    back_office_path = "back_office.csv.gz"
    day1_path = "17-10-2018.3880.gz"
    day2_path = "18-10-2018.3880.gz"

    # Reading and parsing the data files as Pandas DataFrame
    back_ofis = pd.read_csv(path + back_office_path, sep=",")
    day_one = pd.read_table(path + day1_path, sep=";")
    day_two = pd.read_table(path + day2_path, sep=";")

    return back_ofis, day_one, day_two  # Returns the Raw Data Needed for further Analysis


def process_data():
    # todo exercise 2
    # Defining the important features for the Shops Dataframe
    back_ofis_columns = ['pe_ref_in_enseigne', 'pe_id', 'p_id_cat']
    shop_columns = ['refenseigne', 'refmagasin', 'categorieenseigne', 'prixproduit', 'identifiantproduit']

    # Getting the needed DataFrame for the Merging operation
    back_ofis, day_one, day_two = import_raw_data()

    # Extracting the needed features
    back_ofis = back_ofis[back_ofis_columns]
    day_one = day_one[shop_columns]
    day_two = day_two[shop_columns]

    # Merging operation of the Datasets
    back_office_d1 = pd.concat([back_ofis, day_one], axis=1)
    back_office_d2 = pd.concat([back_ofis, day_two], axis=1)

    return back_office_d1, back_office_d2  # Returns the merged Dataset


def average_prices():
    # todo exercise 3
    # Features needed for the Computation
    col1 = 'identifiantproduit'
    col2 = 'prixproduit'
    avg_col1 = 'IdentifiantProduit'
    avg_col2 = 'PrixMoyen'

    # Getting the needed DataFrames
    df1, df2 = process_data()

    # Calculating the Average Prices of the Products
    average_prices_df1 = df1.groupby(col1)[col2].mean()
    average_prices_df2 = df2.groupby(col1)[col2].mean()

    # Inner function to make Dataframe in preparation of writing the result to csv file
    def make_df(df):
        store = dict()   # Initialize a dictionary to store the key, value pairs
        # loop over the calculated Average prices
        for key, value in df.items():
            store[key] = value  # Stores the key, value pair
        # Generating dictionary that will be passed as parameter to the pandas DataFrame
        df_dict = {avg_col1: list(store.keys()), avg_col2: list(store.values())}
        return pd.DataFrame(df_dict)  # Returns the DataFrame

    # Making Dataframe of the Average values
    df1_csv = make_df(average_prices_df1)
    df2_csv = make_df(average_prices_df2)

    # Writing the results in .csv file
    df1_csv.to_csv('DayOneAverageProductPrice.csv')
    df2_csv.to_csv('DayTwoAverageProductPrice.csv')

    return average_prices_df1, average_prices_df2  # Returns the Average Product Prices


def count_products_by_categories_by_day():
    # todo exercise 4
    # Getting the needed features
    col_1 = 'p_id_cat'  # Data Impact's Product Category Column Identifier
    col_2 = 'categorieenseigne'  # Retailer's Product Category Column Identifier

    # Getting the needed DataFrames
    day1_df, day2_df = process_data()

    # Number of Products in each Category in shops for the two days
    di_prod_cat_d1 = len(day1_df.groupby(col_1)[col_1].count())  # DataImpact's Product Category for Day One
    di_prod_cat_d2 = len(day2_df.groupby(col_1)[col_1].count())  # DataImpact's Product Category for Day Two

    ret_prod_cat_d1 = len(day1_df.groupby(col_2)[col_2].count())  # Retailer's Product Category for Day One
    ret_prod_cat_d2 = len(day2_df.groupby(col_2)[col_2].count())  # Retailer's Product Category for Day Two

    return di_prod_cat_d1, di_prod_cat_d2, ret_prod_cat_d1, ret_prod_cat_d2


def average_products_by_categories():
    # todo exercise 4
    # Getting the needed features
    di_pid = 'pe_id'
    di_cat_id = 'p_id_cat'
    ret_id = 'identifiantproduit'
    ret_cat_id = 'categorieenseigne'

    # Getting the Data for the Calculation
    day1_df, day2_df = process_data()

    # Calculating the average # products per DataImpact Category
    dataimpact_avg_numOfProd_d1 = len(day1_df.groupby([di_pid])[di_cat_id]) / len(day1_df[di_cat_id].unique())
    dataimpact_avg_numOfProd_d2 = len(day2_df.groupby([di_pid])[di_cat_id]) / len(day1_df[di_cat_id].unique())

    # Calculating the average # products per Retailer Category
    retailer_avg_numOfProd_d1 = len(day1_df.groupby([ret_id])[ret_cat_id])/len(day1_df[ret_cat_id].unique())
    retailer_avg_numOfProd_d2 = len(day2_df.groupby([ret_id])[ret_cat_id])/len(day2_df[ret_cat_id].unique())

    # Returns the Average Product per Category for DataImpact and Retailer categories for both days
    return dataimpact_avg_numOfProd_d1, dataimpact_avg_numOfProd_d2, retailer_avg_numOfProd_d1, retailer_avg_numOfProd_d2
